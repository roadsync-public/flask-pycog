import boto3
import pytest
from requests import request
import flask_pycog
import requests_mock

from cryptography.hazmat.backends.openssl.rsa import _RSAPublicKey
from moto import mock_ssm

JWK_FIXTURE = {
    "keys": [
        {
            "alg": "RS256",
            "e": "AQAB",
            "kid": "Rlqwp2c2xBRe0FwOJ8daqdSuma8Lfzzo8Tj+Uazm+LQ=",
            "kty": "RSA",
            "n": "0bRi2YIE9bhMpk9P9A_fYBdRBubr0GlbgLrCKG4hAXGqMrf_HZBF14Pd-5SpkkBRnAJK_-2qS11YWHheu66RtN84XoqX7eZQMz09q6fRRlbTDmGBpYlj5n51I1WEUScMuiklc63uuOWgkVYB3BFuzy7TC1RGD9xdlwi9eD_sdi32sajWCF1-viNMVMsidwpX1NensODTqJtIhBqQ2VZoz42mlpCxivCPcxyV-meH2J6nMN5l7WG02ejZh6y7dnf2N62r85WDC7NItYWThSWP3P4gD9pBJwqDEf57Z5iRtTLc_aFsuhUOxxcvThC66PfUvodo8RUnqdSReFNYhxnyZw",
            "use": "sig"
        },
        {
            "alg": "RS256",
            "e": "AQAB",
            "kid": "/w0mS44LA5eE7dqt423YpuIOK8mINKu7sQsaS18SWVE=",
            "kty": "RSA",
            "n": "zSDi1_kyrqh_9C7I_eztq9ET5635I0UcTQNIq_w3AhgL_fGr4Xm7dK4aWNp6jmiA1Uhe-LussB2gIe66HhRqyFZF-OlXMdZU9Lp2_41kP4sbqnT69DfgHQPWLpKlM9_6d_fBvNoJC0TP4ZEG3JiOHn3sLMbE3bfjeQTegw4AUrx-ICAfAEk9FvAltrVRgkl1R7UlfqLDJS8Pv4NkiV5d0U4l6Yc9cN6sOmdZZABSrXhbitLQVSS_ZnM4nUCozMToobi_2o6ye3ba7XNfnRYhA6jGgIke2BthLzTMNTeZ7QcbqHwU1Nm-_k73J01YIugX4sC47F3JFYsr_E6netwcuw",
            "use": "sig"
        }
    ]
}

PARSED_JWK = {
    
}


@pytest.fixture
def mocked_ssm():
    with mock_ssm():
        client = boto3.client('ssm', region_name='us-east-1')
        client.put_parameter(
            Name='/test/ssm',
            Value='us-east-1_ABCTEST1'
        )
        yield


@pytest.fixture
def mocked_fetch_response():
    pass


def init_app():   
    pass


def test_get_ssm_value(mocked_ssm):
    expect = 'us-east-1_ABCTEST1'
    value = flask_pycog.cognito.get_ssm_value('/test/ssm')
    assert value == expect


def test_load_cognito_keys():
    with requests_mock.Mocker() as m:
        m.get(requests_mock.ANY, json=JWK_FIXTURE)
        result = flask_pycog.cognito.load_cognito_keys('TEST-REGION_ABC123')

        # Make sure we slice-n-diced the region and used it with the URL properly
        assert len(m.request_history) > 0
        expected_url = flask_pycog.cognito.JWK_URL.format(region='test-region', userPoolId='TEST-REGION_ABC123')
        assert m.request_history[0].url == expected_url
        # Make sure it passed through cleanly
        assert result == JWK_FIXTURE


def test_repack_jwks():
        result = flask_pycog.core.repack_jwks(JWK_FIXTURE)
        assert len(result) == 2
    
        key_1 = JWK_FIXTURE['keys'][0]
        kid_1 = key_1['kid']
        assert kid_1 in result
        assert result[kid_1]['alg'] == key_1['alg']
        assert isinstance(result[kid_1]['key'], _RSAPublicKey)
        
        key_2 = JWK_FIXTURE['keys'][1]
        kid_2 = key_2['kid']
        assert kid_2 in result
        assert result[kid_2]['alg'] == key_2['alg']
        assert isinstance(result[kid_2]['key'], _RSAPublicKey)
