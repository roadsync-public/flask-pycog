import logging
import boto3
import requests
from flask_pycog.constants import AWS_REGION

log = logging.getLogger(__name__)

JWK_URL = "https://cognito-idp.{region}.amazonaws.com/{userPoolId}/.well-known/jwks.json"


def region_from_pool_id(user_pool_id: str):
    region = user_pool_id.split('_')[0]
    return region


def load_cognito_keys(user_pool_id: str) -> dict:
    """
    Fetch the JWKs from Cognito for use in verification

    :arg user_pool_id: str AWS Cognito user_pool_id, required
    """
    log.debug(f"Fetching cognito keys for pool {user_pool_id}")
    region = region_from_pool_id(user_pool_id)

    request = requests.get(
        JWK_URL.format(
            region=region, 
            userPoolId=user_pool_id
        )
    )
    if not request.ok:
        try:
            request.raise_for_status()
        except Exception as e:
            log.exception(
                f"Exception trying to fetch cognito keys from {region}:{user_pool_id}"
            )
            raise

    return request.json()


def get_cognito_key(kid: str, key_pool: list) -> dict:
    """
    Given the kid claim from a JWT header, grab the correct JWK for verification

    :arg kid: str the key ID (kid) from the JWT header
    :arg key_pool: dict A pool of keys to grab the key from
    """
    if kid not in key_pool:
        raise KeyError(f"Could not find kid[{kid}] in given key pool")
    return key_pool[kid]


def get_ssm_value(key: str, decrypt: bool = False, region: str = AWS_REGION) -> str:
    """
    Given an AWS SSM configuration key, fetch the value

    :arg key: str AWS SSM configuration key
    :arg decrypt: bool Attempt to decrypt param or not, default False
    """
    ssm = boto3.client('ssm', region_name=region)
    try:
        parameter = ssm.get_parameter(Name=key, WithDecryption=decrypt)
        if not parameter:
            raise ValueError(f"Could not fetch SSM param with name '{key}'")
        return parameter['Parameter']['Value']
    except Exception as e:
        log.exception(f"Exception while trying to fetch SSM value '{key}'")
