__version__ = '1.2.2'

from flask_pycog.core import (
    init_cognito,
    init_from_ssm,
    init_jwt_manager,
    decode_jwt,
    verify_jwt
)
