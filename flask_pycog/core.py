import logging
import json
import os
from flask_pycog.constants import AWS_REGION
import jwt

from flask import jsonify
from werkzeug.exceptions import (
     Unauthorized,
     InternalServerError,
)

from flask_pycog.cognito import (
    get_cognito_key,
    get_ssm_value,
    load_cognito_keys,
    region_from_pool_id
)

try:
    # Allow for optional use of Flask-JWT-Extended
    from flask_jwt_extended import JWTManager
except ImportError:
    JWTManager = None


log = logging.getLogger(__name__)

REGION=AWS_REGION
KEY_POOL = None # Dictionary of public keys with the kid as the key, default None


def repack_jwks(jwks: dict) -> dict:
    '''
    '''
    public_keys = {}
    for jwk in jwks['keys']:
        kid = jwk['kid']
        public_keys[kid] = {
            "key": jwt.algorithms.RSAAlgorithm.from_jwk(
                json.dumps(jwk)
            ),
            "alg": jwk['alg']
        }
    return public_keys


def decode_jwt(raw_jwt: str, verify: bool = True, **kwargs) -> dict:
    '''
    Decode the given raw JWT against the cognito JWKs

    :arg raw_jwt: str The raw JWT
    :arg verify: bool Verify the signature before returning, default True
    :kwargs: Additional keyword args based allowed by jwt.decode_complete from pyJWT
    '''
    global KEY_POOL
    
    if KEY_POOL is None:
        log.error('KEY_POOL is empty, please run `init_cognito` before using this function')
        raise InternalServerError("Server configuration incomplete")

    try:
        unverified = jwt.api_jwt.decode_complete(
            raw_jwt,
            verify_signature=False,
            verify_exp=False,
        )
    except:
        log.exception('Failed to decode unverified token')
        raise Unauthorized("The given bearer token could not be decoded")

    if not verify:
        return unverified

    key = get_cognito_key(
        unverified['header']['kid']
    )

    try:
        decoded = jwt.decode_complete(
            raw_jwt, 
            key['key'], 
            algorithms=[key['alg'], ],
            verify_signature=True,
            **kwargs
        )
        return decoded
    except:
        log.exception('Failed decode and verify given token')
        raise Unauthorized("The given bearer token is not valid")


def verify_jwt(raw_jwt: str, **kwargs) -> bool:
    '''
    Verify the given raw JWT against the JWK

    :arg raw_jwt: The raw JWT
    :kwargs: Additional keyword arguments accepted by `jwt.decode_complete` from pyJWT
    '''
    try:
        decode_jwt(raw_jwt, verify=True, **kwargs)
        return True
    except:
        return False


def init_jwt_manager(_jwt: "JWTManager") -> None:
    '''
    Initialize integration with flask_jwt_extended.

    This function will hook into the `JWTManager.decode_key_loader` to automatically 
    pick the correct JWK to decode and verify the signature for a given JWT.

    This method is automatically called when a `JWTManager` is provided to `init_cognito`
    and does not need to be called separately unless `init_cognito` is called before
    providing `JWTManager`.

    You *must* call `init_cognito` before the integration with flask_jwt_extended will work.

    :args _jwt: JWTManager instance
    :returns: None    
    '''

    log.debug("Installing `decode_key_loader` hook for flask-jwt-extended integration")

    @_jwt.decode_key_loader
    def pick_jwt_secret(header, payload):
        global KEY_POOL
        try:
            key = get_cognito_key(
                header['kid'],
                KEY_POOL
            )
            return key['key']
        except KeyError:
            raise Unauthorized("The given bearer token is not valid, please make sure it was created for this environment")
        except:
            log.exception("Unable to get the cognito key for the given JWT")
            raise Unauthorized("The giben bearer token is not valid")

    log.info("Installing `user_lookup_error_loader` hook for flask-jwt-extended integration")

    @_jwt.user_lookup_error_loader
    def handle_user_lookup_error(header, payload):
        return jsonify(message="A valid token is required"), 401

    log.info("Installing `expired_token_loader` hook for flask-jwt-extended integration")

    @_jwt.expired_token_loader
    def handle_expired_token(header, payload):
        log.warning("Given token has expired")
        return jsonify(message="Token is expired or otherwise invalid"), 401
    
    log.info("Installing `invalid_token_loader` hook for flask-jwt-extended integration")

    @_jwt.invalid_token_loader
    def handle_invalid_token(reason):
        log.warning(f"Given token is invalid: {reason}")
        return jsonify(message="A valid token is required"), 401
    
    log.info("Installing `unauthorized_loader` hook for flask-jwt-extended integration")
    
    @_jwt.unauthorized_loader
    def handle_unauth(reason):
        log.warning(f"JWT auth failure: {reason}")
        return jsonify(message="A valid token is required"), 401
    
    @_jwt.token_verification_failed_loader
    def handle_token_verification_error(header, payload):
        log.warning("Given token could not be verified")
        return jsonify(message="A valid token is required"), 401

def init_from_ssm(ssm_key: str, _jwt: "JWTManager"=None, region=REGION):
    '''
    Initializes cognito handling after first fetching the user_pool_id from a given AWS SSM key.
    
    :arg ssm_key: str AWS SSM secret name to fetch the user_pool_id from (Redundant if user_pool_id provided)
    :arg _jwt: JWTManager or None
    '''

    log.debug(f"Fetching SSM value for {ssm_key}")
    user_pool_id = get_ssm_value(ssm_key, region=region)
    log.debug(f"user_pool_id: {user_pool_id}")
    init_cognito(
        user_pool_id,
        _jwt,
    )

def init_cognito(
    user_pool_id: str,
    _jwt: "JWTManager" = None
    ) -> None:
    '''
    Initialize cognito handling. Pass JWTManager to automatically integrate cognito with flask_jwt_extended.

    Note: AWS Region is parsed from the `user_pool_id`

    :arg user_pool_id: str AWS Cognito User Pool (required)
    :arg _jwt: JWTManager or None
    '''
    global KEY_POOL
    global REGION
    
    if not user_pool_id:
        raise ValueError('A user_pool_id is required')

    log.debug('Initializing cognito integration')

    # The Cognito user pool ID provides the region, double check it
    REGION = region_from_pool_id(user_pool_id)
    log.debug(f"Parsed region [{REGION}] from the user_pool_id")
    raw_jwks = load_cognito_keys(
        user_pool_id
    )
    log.debug(f"Retrieved {len(raw_jwks)} keys from Cognito")
    KEY_POOL = repack_jwks(raw_jwks)

    if JWTManager is not None:
        if isinstance(_jwt, JWTManager):
            init_jwt_manager(_jwt)
        else:
            log.warning(
                'flask_jwt_extended is installed but `init_cognito` was called '
                'without a JWTManager instance and will not integrate with flask_jwt_extended'
            )
