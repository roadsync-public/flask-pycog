===========
Flask-PyCog
===========

This library provides a simple way to integrate Cognito-based bearer tokens (JWT) with or without the flask-jwt-extended library.

The goal is to provide basic use of AWS Cognito bearer tokens without implementing the entire authentication flow which is helpful for
use with internal systems or distributed microservices.

Installation
============
```sh
pip install poetry add git+https://gitlab.com/roadsync-public/flask-pycog.git@v1.2.1
```
or
```sh
poetry add git+https://gitlab.com/roadsync-public/flask-pycog.git@v1.2.1
```

By default `flask_pycog` does not install `flask_jwt_extended` when you use `pip` or `poetry` for installation.
If you want to install `flask_jwt_extended` then you need to add `[all]`, e.g. `pip install package@1.2.3[all]`.

.. _Configuration:

Configuration
-------------

Configuration of the library is handled by using `init_cognito`_ described below if you intend to directly have the Cognito user Pool ID for immediate use.
If you intend on using AWS SSM to dynamically fetch the Pool ID the `init_from_ssm`_ method should be used instead.

In cases where you may want to initialize the integration with `flask_jwt_extended` separately from `init_cognito`
you may use `init_jwt_manager`_ directly instead of passing a `JWTManager` instance to `init_cognito`.

In *all* cases, you *must* use `init_cognito` before attempting to use the library functionality.

When you are using `flask_jwt_extended` you will need to add the following to your FlaskApp configuration.

```
app.config['PROPAGATE_EXCEPTIONS'] = True # Allows for proper error handling
app.config['JWT_DECODE_ALOGRITHMS'] = ['RS256'] # Sets the JWT Algorithm to expect
```

Functions
=========

.. _init_cognito:

init_cognito
------------

Signature: ``init_cognito(user_pool_id: str, _jwt: "JWTManager" = None) -> None```
Returns: None

Configure the flask-pycog integration, required before using anything else from the library.

.. list-table::
    :widths: 25 25 25 50
    :header-rows: 1

    * - Argument
      - Type
      - Default
      - Description
    * - user_pool_id
      - string
      - None
      - Required
    * - _jwt
      - JWTManager
      - None
      - Instance of the JWT-Flask-Extended JWTManager being used by your Flask app. Optional if not using Flask-JWT-Extended.
    
.. _init_from_ssm:

init_from_ssm
-------------

Signature: ``init_from_ssm(ssm_key: str, _jwt: "JWTManager" = None, region='us-east-1') -> None```
Returns: None

Alternatively you may configure the cognito integration by providing an AWS SSM key name to fetch the Pool ID from dynamically.

.. list-table::
    :widths: 25 25 25 50
    :header-rows: 1

    * - Argument
      - Type
      - Default
      - Description
    * - ssm_key
      - string
      - Required
      - AWS SSM key to dynamically retrieve the pool ID from
    * - _jwt
      - JWTManager
      - None
      - Instance of the JWT-Flask-Extended JWTManager being used by your Flask app. Optional if not using Flask-JWT-Extended.
    * - region
      - string
      - us-east-1
      - The region for the given SSM key


.. _init_jwt_manager:

init_jwt_manager
----------------

Signature: ``init_jwt_manager(_jwt: "JWTManager" = None) -> None```
eturns: None

Late initialization for the integration with flask_jwt_extended.

This function will hook into the `JWTManager.decode_key_loader` to automatically pick the correct JWK to decode and verify the signature for a given JWT.

This method is automatically called when a `JWTManager` is provided to `init_cognito` and does not need to be called separately *unless* `init_cognito` 
is called before providing a `JWTManager` instance.

.. list-table::
    :widths: 25 25 25 50
    :header-rows: 1

    * - Argument
      - Type
      - Default
      - Description
    * - jwt
      - JWTManager
      - Required
      - Instance of the JWT-Flask-Extended JWTManager being used by your Flask app. Pass None if used outside of Flask-jwt-extended.

decode_jwt
----------

Signature: ``decode_jwt(raw_jwt, verify=True, **kwargs) -> dict``
Returns: Dictionary with "header", "payload", and "signature" keys, values are the corresponding claims/values

Optional function to manually decode a given JWT, good to use if not integrating with flask-jwt-extended.

This is a wrapper function around the underlying `decode_complete` function provided by `PyJWT`.

See `the PyJWT docs for additional kwargs available. <https://pyjwt.readthedocs.io/en/stable/api.html?highlight=decode_complete#jwt.decode_complete>`_

.. list-table::
    :widths: 25 25 25 50
    :header-rows: 1

    * - Argument
      - Type
      - Default
      - Description
    * - raw_jwt
      - string
      - required
      - Raw JWT to decode
    * - verify
      - boolean
      - True
      - Whether to provide the raw decoded JWT or require validation first
    * - \*\*kwargs
      - additional keyword args
      - optional
      - Keyword arguments that would be passed into `jwt.decode_complete` from `pyjwt`

Returns::
    
    Example:
    {
        "header": {
            "kid": "ABC1234",
            "alg": "RSA256"
        },
        "payload": {
            "iat": "123456789",
            "iss": "Some Issuer",
            "sub": "Subject here",
            "username": "SomeName"
        },
        "signature": {
            "kty": "RSA", # etc..
        }
        ""
    }

verify_jwt
----------

Signature: ``verify_jwt(raw_token, **kwargs) -> bool``
Returns: Boolean

Optional function to erify the signature a given JWT, good to use if not integrating with flask-jwt-extended.

This is a wrapper function around the underlying `decode_complete` function provided by `PyJWT`.

See `the PyJWT docs for additional kwargs available. <https://pyjwt.readthedocs.io/en/stable/api.html?highlight=decode_complete#jwt.decode_complete>`_

.. list-table::
    :widths: 25 25 25 50
    :header-rows: 1

    * - Argument
      - Type
      - Default
      - Description
    * - raw_jwt
      - string
      - required
      - Raw JWT to decode
    * \*\*kwargs
      - additional keyword args
      - optional
      - Keyword arguments that would be passed into `jwt.decode_complete` from `pyjwt`


Usage
=====

With flask-jwt-extended
------------------------

Simply import `init_cognito`` from `flask-pycog` and provide the necessary config.

When using flask-jwt-extended, the library will automatically hook into the jwt library to provide
the proper JWT signature key for verification. No additional work is necessary!

Example::

    init_cognito(
        jwt, # JWTManager instance
        user_pool_id='us-east-1_SomePoolID'
    )

Without flask-jwt-extended
--------------------------

If you are using the library but do not want to use flask-jwt-extended, you may still do so using the provided verify and decode functions.

Example::

    from flask-pycog import init_cognito, decode_jwt, verify_jwt

    # You still need to initialize with init_cognito, but provide None instead of the manager
    init_cognito(
        None,
        user_pool_id='us-east-1_SomePoolID'
    )

    token = '<your token here>'
    # Verify if the token is valid or not, returns True/False
    valid = verify_jwt(token)
    # Decode the token after verification
    decode_verify = decode_jwt(token)
    # Decode the token without verification
    decode_no_verify = decode_jwt(token, verify=False)


Example simple apps
===================

Simplest example with explicit user_pool_id::

    from flask import Flask
    from flask_pycog import init_cognito
    from flask_jwt_extended import JWTMananger

    def create_app():
        # Set up a flask app and the jwt manager as normal
        app = Flask(__name__)

        app.config['APPLICATION_ROOT'] = '/'
        app.config['JWT_DECODE_ALOGRITHMS'] = ['RS256']
        
        # Set up JWTManager and cognito
        jwt = JWTManager(app)
        # Initialize the flask-pycog integration
        init_cognito(
            user_pool_id='us-east-1_SomePoolID',
            jwt,
        )

Getting user_pool_id from AWS SSM::

    from flask import Flask
    from flask_pycog import init_from_ssm
    from flask_jwt_extended import JWTMananger

    def create_app():
        # Set up a flask app and the jwt manager as normal
        app = Flask(__name__)
        jwt = JWTManager(app)
        # Initialize the flask-pycog integration
        init_from_ssm(
            ssm_key='/my/config/name',
            jwt
        )
