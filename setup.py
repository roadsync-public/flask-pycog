from setuptools import setup

setup(
    name='Flask-PyCog',
    description='Simple plugin to allow flask-jwt-extended to handle cognito keys and tokens',
    packages=['flask_pycog'],
    include_package_data=True,
    install_requires=[
        'boto3',
        'flask-jwt-extended',
        'pyjwt[crypto]',
        'requests'
    ],
    entry_points = {
        'console_scripts': [
        ]
    }
)
